# -*- encoding: utf-8 -*-
import pytest
import xmltodict

from http import HTTPStatus

from block.tests.factories import PageFactory


@pytest.mark.django_db
def test_site_map(client):
    PageFactory(slug="apple", slug_menu="orange")
    PageFactory(slug="kb", slug_menu="software")
    response = client.get("/sitemap.xml")
    assert HTTPStatus.OK == response.status_code
    result = xmltodict.parse(response.content)
    data = result["urlset"]["url"]
    assert {
        "http://testserver/kb/software/",
        "http://testserver/apple/orange/",
    } == set([x["loc"] for x in data])
