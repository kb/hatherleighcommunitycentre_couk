# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib.sitemaps import GenericSitemap
from django.contrib.sitemaps.views import sitemap
from django.urls import re_path

from block.models import Page


info_dict = {
    "queryset": Page.objects.pages(),
    "date_field": "modified",
}

sitemaps = {
    "block": GenericSitemap(info_dict, priority=0.5, changefreq="monthly"),
}

urlpatterns = [
    re_path(r"^sitemap\.xml$", view=sitemap, kwargs={"sitemaps": sitemaps}),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^block/", view=include("block.urls.block")),
    re_path(r"^compose/", view=include("compose.urls.compose")),
    re_path(r"^dash/", view=include("dash.urls")),
    re_path(r"^gallery/", view=include("gallery.urls")),
    re_path(r"^wizard/", view=include("block.urls.wizard")),
    # this url include should come last
    re_path(r"^", view=include("block.urls.cms")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = (
        [re_path(r"^__debug__/", include(debug_toolbar.urls))]
        + urlpatterns
        + static("/scss", document_root="scss")
    )
