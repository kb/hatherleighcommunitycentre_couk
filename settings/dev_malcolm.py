# -*- encoding: utf-8 -*-
from .local import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "dev_www_hatherleighcommunitycentre_co_uk_malcolm",
        "USER": "malcolm",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}
